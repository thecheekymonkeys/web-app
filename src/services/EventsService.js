const api = 'http://localhost:8080/graphql';

export const getEvents = (data) =>  {
    return fetch(api, {
        method: 'POST',
        headers: { 
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
        mode: 'cors',
        body: JSON.stringify({
            query: `{
              events(date: "2017-12-03") {
                id,
                name,
                address,
                description,
                url
              }
            }`
        })
    })
        .then(res => res.json())
        .catch(err => console.error(err));
}
