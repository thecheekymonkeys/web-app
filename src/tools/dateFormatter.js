const dateFormat = 'dddd MMM YYYY';

export const formatDate = (date) => date.format(dateFormat);
