import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import events from './reducers/events';
import filters from './reducers/filters';

export default createStore(
  combineReducers({ events, filters }),
  applyMiddleware(thunk)
);
