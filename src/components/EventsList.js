import React, {Component} from 'react';
import { connect } from 'react-redux';
import EventsListItem from './EventsListItem';
import { fetchEvents } from '../actions/events';

class EventList extends Component {

  componentDidMount() {
    this.props.fetchEvents();
  }

  render() {
    return (
      <ul>
        { this.props.events
            .map(event => <EventsListItem {...event} />) 
        }
      </ul>
    );
  }
}

export default connect(
  (state) => ({ events: state.events }),
  { fetchEvents }
)(EventList);
