import React from 'react';
import DOMPurify from 'dompurify';
import { Card, Col } from 'react-materialize';

export default ({ id, name, description, local_date, url }) => (
<li key={id}>
    <Col m={3} s={6}>
        <Card className='card' textClassName='' title={name} actions={[<a href={url}>Read More</a>]}>
            <div className='content' dangerouslySetInnerHTML={{__html: DOMPurify.sanitize(description)}}></div>
        </Card>
    </Col>
</li>
);

