import React from 'react';
import { connect } from 'react-redux';

import { changeDateFilter } from '../actions/filters';
import FiltersDate from './FiltersDate';

class Filters extends React.Component {

  render(props, actions) {
    return <div className="filters">
      <FiltersDate  
        handleChange={this.props.changeDateFilter}
      />
      <p>Showing events for <span style={{'fontWeight': 'bold'}}>{this.props.date}</span></p>
    </div>
  }

}

export default connect(
  (state) => ({ date: state.filters.date }),
  { changeDateFilter }
)(Filters);
