import React from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import 'react-datepicker/dist/react-datepicker.css';

class FiltersDate extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      date: props.date,
      dateObj: moment()
    };
    this.handleChangeCallback = props.handleChange;
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(dateObj) {
    this.setState({
      dateObj
    });
    this.handleChangeCallback(dateObj);
  }

  render() {
    return <DatePicker 
      selected={this.state.dateObj} 
      onChange={this.handleChange} />
  }

}

export default FiltersDate;
