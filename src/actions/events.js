import { getEvents } from '../services/EventsService';
import { GET_EVENTS } from '../types/events';

export const fetchEvents = (date) => (dispatch, getState) => 
  getEvents(date)
        .then(res => dispatch({ type: GET_EVENTS, payload: res.data.events }))
        .catch(err => console.error(err))

