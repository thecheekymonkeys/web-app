import { formatDate } from '../tools/dateFormatter';
import CHANGE_DATE_FILTER from '../types/filters';

export const changeDateFilter = (payload) => 
({ type: CHANGE_DATE_FILTER, payload: formatDate(payload) })
