export default (state = [], {type, payload}) => {

  switch(type) {

    case 'GET_EVENTS':
      return payload || state;

    default:
      return state;
      
  }

};
