import CHANGE_DATE_FILTER from '../types/filters';

const initialState = {
  date: '',
};

export default (state = initialState, {type, payload}) => {

  switch(type) {

    case CHANGE_DATE_FILTER:
      return {
        ...state,
        date: payload || state.date
      };

    default:
      return state;

  }

}
