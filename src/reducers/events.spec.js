import reducer from './events';

const events = [{
  name: 'Hello',
  description: 'Darkness'
}];

describe('Events Reducer', () => {

  test('returns a state object', () => 
    expect(reducer(undefined, { type: 'ANY' })).toBeDefined()
  );

  test('replace list of events', () => 
    expect(reducer(undefined, { type: 'GET_EVENTS', payload: events })).toEqual(events)
  );

});
