import React, { Component } from 'react';
import './App.css';
import headerLogo from './bgImage.svg';
import EventsList from './components/EventsList';
import Filters from './components/Filters';

class App extends Component {
  render() {
    return (
      <div className="app">
        <header className="header">
          <img className="header-logo" src={headerLogo} alt=":(" />
        </header>
        <Filters />
        <div className="app-body container">
          <EventsList />
        </div>
      </div>
    );
  }
}

export default App

